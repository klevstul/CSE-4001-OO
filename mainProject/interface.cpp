#include "interface.h"

Interface::Interface()
{
	cout<<"*** ************************************************* ***\n";
	cout<<"*** picturePerfecte Newspaper Typesetting Application ***\n";
	cout<<"*** ************************************************* ***\n\n";

}



Interface::~Interface()
{
	delete newsObj;
}

void Interface::setupNewspaper()
{
	char ch;
	cout<<"Type the process you want to build the Newspaper:\n";
	cout<<"1. Auto building\n2. Manual building\n";
	cout<<"Your choice : ";
	cin>>ch;
	if (ch == '1')
	{
		newsObj = new Newspaper(1);
		newsObj->autoBuild();
	}	
	else 
	{

		newsObj = new Newspaper(2);	
		newsObj->createPages();
	}

}

int Interface::buildPages()
{
	return newsObj->createPages();
}
