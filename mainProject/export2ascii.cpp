#include "export2ascii.h"
#include<iostream>
#include<string>
#include<sstream>
#include <stdio.h>
using namespace std;

// ---------------------------------------------------
// file:	export2ascii.cpp
// author:	Frode Klevstul (frode@klevstul.com)
// started:	20030607
// ---------------------------------------------------

Export2ASCII::Export2ASCII(){}

Export2ASCII::~Export2ASCII(){}

string Export2ASCII::parseContent(string content, int pagewidth, int pageheight, bool showoutput) const {
	string columns [10];				// the columns for each page (the raw text) is stored here
	string articles [10];				// conaining articles for each column
	string page;						// a string containing the raw text
	int columnno = 0;					// number of columns on a page
	int pageno = 0;						// pagenumber
	int i;								// helpcounter
	int y;								// helpcounter
	int tmpcolno;						// helpcounter
	string output = "";					// the output we will write to file
	string tmp;							// helpstring
	int outputpagewidth;				// the pagewidth of our output page
	int outputpageheight;				// the pageheigh of our output page
	int columnwidth;					// the width of a column
	string border = "*";				// the character representing a border
	string pillar = " "+border+" "; 	// the string representing a pillar
	int pillarwidth = pillar.length();	// the width of a pillar


//	cout << "DEBUG: parseContent: ASCII" << endl;
//	cout << "DEBUG: page width: " << pagewidth << endl;
//	cout << "DEBUG: page height: " << pageheight << endl;

	// substitute continuing tags
	sustituteContTags(content);


	// -----------------------
	// go through all pages
	// -----------------------
	while ( hasPage(content) ){
		page = getPage(content);
		pageno = getPagenumber(page);
		
		// -----------------------
		// go through all columns
		// -----------------------
		columnno = 0;
		while ( hasColumn(page) ){
			columns[columnno] = getColumn(page);
			
			// -----------------------
			// go through all articles
			// -----------------------
			while ( hasArticle(columns[columnno]) ){
				articles[columnno] += substituteTags(getArticle(columns[columnno]));
				//cout << "column#" << columnno << " content:\n" <<  articles[columnno] << endl;
			}
			columnno++;			
		}
		
		outputpageheight = pageheight+2;
		columnwidth = pagewidth/columnno;
		outputpagewidth = (columnno * columnwidth) + ((columnno+1) * pillarwidth);
		
		//cout << "pagewidth: " << pagewidth << endl;
		//cout << "columnno: " << columnno << endl;
		//cout << "output pageheight: " << outputpageheight << endl;
		//cout << "output pagewidth: " << outputpagewidth << endl;
		//cout << "output columnwidth: " << columnwidth << endl;


		for (i=0; i<outputpageheight; i++){
			
			// firstline and lastline, print out layout (heading and end of page)
			if (i==0 || i==(outputpageheight-1)){
				tmp = "";
				for (y=0;y<outputpagewidth;y++){
					if (y==0||y==outputpagewidth-1)
						tmp += " ";
					else
						tmp += border;
				}
				output += tmp+"\n";
				
				// prints out pagenumber after last line
				if (i==outputpageheight-1){
					// convert int to string
					stringstream s;
					s << pageno;
					tmp = s.str();

					output += " Page #"+tmp+"\n\n\n";
				}

			// normal lines, print out articles for this page...
			} else {
						
				for (tmpcolno=0; tmpcolno<columnno; tmpcolno++){
					if (tmpcolno==columnno-1)
						output += pillar + getLine(articles[tmpcolno],columnwidth) + pillar + "\n";
					else
						output += pillar + getLine(articles[tmpcolno],columnwidth);
				}

			}
		}
	}

	if (showoutput)
		cout << endl << output << endl;

	return output;
}


bool Export2ASCII::createOutputfile(string filename, string content) const {
	int i;
	FILE *file;
	const char *fname;
	const char *filecontent = content.c_str();
	
	i = filename.find(".txt");
	if (i<0){
		filename += ".txt";
	}	
	filename = "output/"+filename;
	
	// convert string to char* (because of "fopen()")
	fname = filename.c_str();

	file = fopen(fname,"w");
	fprintf(file,"%s",filecontent);
	fclose(file);
	
	return true;
}




string Export2ASCII::substituteTags(string article) const {

	string str = article;
	string articleno;
	string picture = ".----.[br]|    |[br]|    |[br]|    |[br]|____|[br]";
	int i;
	int y;

	// -----
	// find articlenumber
	// -----
	i = str.find("[article#");
	if (i>=0){
		i+=9;
		articleno = str.substr(i,article.length());
		i = articleno.find("]");
		articleno = articleno.substr(0,i);
		i = str.find("]");
		i++;
		str = str.substr(i, article.length());
	}

	// -----
	// find continued
	// -----
	i = str.find("[continued#");
	if (i>=0){
		str.replace(i,11,"(cont. from page ");
		y = str.find("]");
		str.replace(y,1,")");
	}

	// -----
	// find continuing
	// -----
	i = str.find("[continuing#");
	if (i>=0){
		str.replace(i,12,"(cont. on page ");
		y = str.substr(i+4,str.length()).find("]");
		y+=4+i;
		str.replace(y,1,")");
	}

	// -----
	// find title
	// -----
	i = str.find("[title]");
	if (i>=0){
		str.replace(i,7,"");
	}

	// -----
	// find end of title
	// -----
	i = str.find("[/title]");
	if (i>=0){
		str.replace(i,8,"");
	}

	// -----
	// find ctitle
	// -----
	i = str.find("[ctitle]");
	if (i>=0){
		str.replace(i,8,"");
	}

	// -----
	// find end of title
	// -----
	i = str.find("[/ctitle]");
	if (i>=0){
		str.replace(i,9,"");
	}


	// -----
	// find picture
	// -----
	i = str.find("[pic]");
	if (i>=0){
		y = str.find("[/pic]");
		y+=6;
		y = y-i;
		str.replace(i,y,picture);
	}

	// -----
	// find body
	// -----
	i = str.find("[body]");
	if (i>=0){
		str.replace(i,6,"");
	}

	// -----
	// find /body
	// -----
	i = str.find("[/body]");
	if (i>=0){
		str.replace(i,7,"");
	}

	// -----
	// find /article
	// -----
	i = str.find("[/article]");
	if (i>=0){
		str.replace(i,10,"");
	}

	return str;
}




string Export2ASCII::getLine(string& content, int columnwidth) const {
	string str;
	int nospaces;
	int y;
	int i;


	if (content.length() < (columnwidth+3)){
		str = content;
		for (i=content.length();i<columnwidth;i++){
			str+=" ";
		}
		content = str;
	} else {
		str = content.substr(0,columnwidth+3);
	}


	i = str.find("[br]");
	if (i>=0){
		nospaces = columnwidth - i;
		str = str.substr(0,i);
		for (y=0;y<nospaces;y++){
			str+=" ";
		}
		i = content.find("[br]");
		//cout << "i:::" << i << " length(): " << content.length() << " content:\n" << content << endl;
		if (content.length()>4){
			content = content.substr(i+4,content.length());
		}
	} else {
		str = content.substr(0,columnwidth);
		content = content.substr(columnwidth, content.length());
	}

	return str;
}




