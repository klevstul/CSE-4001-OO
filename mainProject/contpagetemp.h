#ifndef CONTPAGETEMP_H_PROTECTOR
#define CONTPAGETEMP_H_PROTECTOR

#include <iostream>
#include "template.h"
#include "articlehandler.h"
using namespace std;


// ---------------------------------------------------
// file:	contpagetemp.h
// author:	Frode Klevstul (frode@klevstul.com)
// started:	20030611
// ---------------------------------------------------

class Contpagetemp : public Template
{
    public:

		Contpagetemp();

		virtual ~Contpagetemp();
	
		string fetchArticles(int pagewidth, int pageheight, int pageno, Articlehandler* articlehandler) const;
		
	private:
	
		int _nocolumns;
		bool _importance[10];

};


#endif
