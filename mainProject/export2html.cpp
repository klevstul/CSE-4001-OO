#include "export2html.h"
#include<iostream>
#include<string>
#include<sstream>
#include <stdio.h>
using namespace std;

// ---------------------------------------------------
// file:	export2html.cpp
// author:	Frode Klevstul (frode@klevstul.com)
// started:	20030606
// ---------------------------------------------------

Export2HTML::Export2HTML(){}

Export2HTML::~Export2HTML(){}

string Export2HTML::parseContent(string content, int pagewidth, int pageheight, bool showoutput) const {
	string output = "";					// the output we will write to file
	string tmpcontent = content;		// used to create indexpage
	string column;						// contains the text of one column
	string article;						// contains an article
	string articleno;					// article number
	string title;						// the title of an article
	string page;						// a string containing the raw text
	string tmp;							// helpstring
	int pageno = 0;						// pagenumber
	int i;								// helpcounter
	int colno;							// number of columns
	int artno = 0;						// article counter


	// substitute continuing tags
	sustituteContTags(content);

	// ----------------------------
	// the start of the HTML page
	// ----------------------------
	output = "<html><head><title>picturePerfect@internet</title></head><link href=\"pP.css\" rel=\"stylesheet\" type=\"text/css\"><body><h1>The CSE Morning Herald (CSEMH)</h1><table><tr><td colspan=\"2\"><b>Index:</b></td></tr>\n";

	// ------------------------------------------------
	// create a index with links to all the articles
	// ------------------------------------------------
	while (hasPage(tmpcontent)){
		page = getPage(tmpcontent);
		colno=0;
		while (hasColumn(page)){
			colno++;
			column = getColumn(page);
			while (hasArticle(column)){
				article = getArticle(column);
				articleno = getArticleno(article);
				title = getTitle(article);
				if (title!=""){
					artno++;
					stringstream s;
					s << artno;
					output += "<tr><td>"+s.str()+":&nbsp;&nbsp;&nbsp;</td><td><a href=\"#article"+articleno+"\">"+title+"</a></td></tr>\n";
				}
			}
		}
	}

	output += "</table><br><br>";

	// ---------------------------------------------
	// create pages with the different articles
	// ---------------------------------------------
	while ( hasPage(content) ){

		page = getPage(content);
		pageno = getPagenumber(page);
		colno = getColNo(page);

		stringstream s;
		s << pageno;
		tmp = s.str();
				
		output += "<a name=\"page"+tmp+"\"><br>\n";
		output += substituteTags(page, pagewidth, pageheight, colno, pageno);
	}

	output += "</body></html>";

	if (showoutput)
		cout << endl << output << endl;

	return output;
}



bool Export2HTML::createOutputfile(string filename, string content) const {
	int i;
	FILE *file;
	const char *fname;
	const char *filecontent = content.c_str();
	
	i = filename.find(".html");
	if (i<0){
		filename += ".html";
	}	
	filename = "output/"+filename;
	
	// convert string to char* (because of "fopen()")
	fname = filename.c_str();

	file = fopen(fname,"w");
	fprintf(file,"%s",filecontent);
	fclose(file);
	
	return true;
}





string Export2HTML::substituteTags(string page, int pagewidth, int pageheight, int colno, int pno) const {

	string str;
	string pageno;
	int i;
	int y;
	int tablewidth = pagewidth * 8;
	int tableheight = pageheight * 10;
	int colwidth = tablewidth/colno;
	string twidth;
	string theight;
	string cwidth;
	stringstream s, s2, s3, s4;

	s << tablewidth;
	twidth = s.str();

	s2 << colwidth;
	cwidth = s2.str();

	s3 << pno;
	pageno = s3.str();
	
	s4 << tableheight;
	theight = s4.str();


	// ---------------------------
	// replace page and /page
	// note: all pages starts with a "[page#xx]" tag, and ends with a "[/page]" tag
	// ---------------------------
	i = page.find("]");
	if (i>=0){
		page.replace(0,i+1,"");
	}

	i = page.find("[/page]");
	if (i>=0){
		page.replace(i,7,"");
	}


	// --------------------------
	// start table
	// --------------------------
	str = "<table border=\"0\" width=\""+twidth+"\" bgcolor=\"#073E07\" cellpadding=\"0\" cellspacing=\"1\"><tr><td bgcolor=\"#073E07\"><font color=\"#aaaaaa\">&nbsp;Page #"+pageno+"</font></td></tr><tr><td><table border=\"0\" width=\"100%\" height=\""+theight+"\" bgcolor=\"#ffffff\" cellpadding=\"2\" cellspacing=\"-1\"><tr>"+page;
				

	// ---------------------------
	// replace column and /column
	// ---------------------------
	i = str.find("[column]");
	while (i>=0){
		str.replace(i,8,"<td width=\""+cwidth+"\" valign=\"top\">");
		i = str.find("[column]");
	}

	i = str.find("[/column]");
	while (i>=0){
		str.replace(i,9,"</td>");
		i = str.find("[/column]");
	}


	// ------------------------
	// create article-anchor
	// ------------------------
	i = str.find("[article#");
	while (i>=0){
		str.replace(i,9,"\n<a name=\"article");

		y = str.substr(i,str.length()).find("]");
		y+=i;
		str.replace(y,1,"\">");
		i = str.find("[article#");
	}


	// ---------------------------
	// replace /article
	// ---------------------------
	i = str.find("[/article]");
	while (i>=0){
		str.replace(i,10,"");
		i = str.find("[/article]");
	}

	// ---------------------------
	// replace title and /title
	// ---------------------------
	i = str.find("[title]");
	while (i>=0){
		str.replace(i,7,"\n<b>");
		i = str.find("[title]");
	}

	i = str.find("[/title]");
	while (i>=0){
		str.replace(i,8,"</b>\n");
		i = str.find("[/title]");
	}


	// ---------------------------
	// replace ctitle and /ctitle (continued titles)
	// ---------------------------
	i = str.find("[ctitle]");
	while (i>=0){
		str.replace(i,8,"\n<b><i>");
		i = str.find("[ctitle]");
	}

	i = str.find("[/ctitle]");
	while (i>=0){
		str.replace(i,9,"</i></b>\n");
		i = str.find("[/ctitle]");
	}



	// ---------------------------
	// replace body and /body
	// ---------------------------
	i = str.find("[body]");
	while (i>=0){
		str.replace(i,6,"");
		i = str.find("[body]");
	}

	i = str.find("[/body]");
	while (i>=0){
		str.replace(i,7,"\n");
		i = str.find("[/body]");
	}

	// ---------------------------
	// replace br
	// ---------------------------
	i = str.find("[br]");
	while (i>=0){
		str.replace(i,4,"<br>\n");
		i = str.find("[br]");
	}

	// -----
	// replace continued
	// -----
	i = str.find("[continued#");
	while (i>=0){
		pageno = str.substr(i+11,str.length());
		y = pageno.find("]");
		pageno = pageno.substr(0,y);
		str.replace(i,11,"<i>cont. from page <a href=\"#page"+pageno+"\">");
		y = str.substr(i,str.length()).find("]");
		y+=i;
		str.replace(y,1,"</a></i>\n");
		i = str.find("[continued#");
	}


	// -----
	// replace continuing
	// -----
	i = str.find("[continuing#");
	while (i>=0){
		pageno = str.substr(i+12,str.length());
		y = pageno.find("]");
		pageno = pageno.substr(0,y);
		str.replace(i,12,"<br><i>cont. on page <a href=\"#page"+pageno+"\">");
		y = str.substr(i,str.length()).find("]");
		y+=i;
		str.replace(y,1,"</a></i>\n");
		i = str.find("[continuing#");
	}

	// ---------------------------
	// replace pic and /pic
	// ---------------------------
	i = str.find("[pic]");
	while (i>=0){
		str.replace(i,5,"<img border=\"1\" src=\"../input/");
		i = str.find("[pic]");
	}

	i = str.find("[/pic]");
	while (i>=0){
		str.replace(i,6,"\">\n");
		i = str.find("[/pic]");
	}


	// --------------------------
	// end the table
	// --------------------------
	str += "</td></tr></table></td></tr></table>";


	return str;
}



string Export2HTML::getArticleno(string article) const {
	string articleno;
	int i;

	i = article.find("[article#");
	if (i>=0){
		i+=9;
		articleno = article.substr(i,article.length());
		i = articleno.find("]");
		articleno = articleno.substr(0,i);
	}

	return articleno;
}


string Export2HTML::getTitle(string article) const {
	string title;
	int i;

	i = article.find("[title]");
	if (i>=0){
		i+=7;
		title = article.substr(i,article.length());
		i = title.find("[/title]");
		title = title.substr(0,i);
	}

	return title;
}



