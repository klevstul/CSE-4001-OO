#ifndef EXPORTERALGORITHM_H_PROTECTOR
#define EXPORTERALGORITHM_H_PROTECTOR

#include <iostream>
#include "exporter.h"
using namespace std;


// ---------------------------------------------------
// file:	exporteralgorithm.h
// author:	Frode Klevstul (frode@klevstul.com)
// started:	20030606
// ---------------------------------------------------

class ExporterAlgorithm : public Exporter
{
    public:

		static ExporterAlgorithm* getExporter(const string& name);

		virtual string parseContent(string content, int pagewidth, int pageheight, bool showoutput) const = 0;

		virtual bool createOutputfile(string filename, string content) const = 0;

		string getColumn(string& content) const;

		bool hasColumn(string& content) const;

		string getPage(string& content) const;

		bool hasPage(string& content) const;

		int getPagenumber(string page) const;

		string getArticle(string& content) const;
		
		bool hasArticle(string& content) const;
		
		void sustituteContTags(string& content) const;

		int getColNo(string page) const;

};


#endif
