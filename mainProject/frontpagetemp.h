#ifndef FRONTPAGETEMP_H_PROTECTOR
#define FRONTPAGETEMP_H_PROTECTOR

#include <iostream>
#include "template.h"
#include "articlehandler.h"
using namespace std;


// ---------------------------------------------------
// file:	frontpagetemp.h
// author:	Frode Klevstul (frode@klevstul.com)
// started:	20030609
// ---------------------------------------------------

class Frontpagetemp : public Template
{
    public:

		Frontpagetemp(int maxsize);

		virtual ~Frontpagetemp();
	
		string fetchArticles(int pagewidth, int pageheight, int pageno, Articlehandler* articlehandler) const;
		
	private:
	
		int _nocolumns;
		bool _importance[10];
		int _maxsize;

};


#endif
