#ifndef ARTICLE_H
#define ARTICLE_H

class Article{

public:
      
      Article( char *heading ="",char *picture ="",char *content="",int importance
      = 0,int size = 0, int bytesLeft = 0 ,int pageNo = 0);
      ~Article();
      char * getHeading();
      char * getPicture();
      char * getContent();
      int getSize();
      int getImportance();
      void setBytes(int bytes);
      int getBytes();
private:
      char *heading;
      char *picture;
      char *content;
      int importance;
      int size;
      int bytesLeft;
      int pageNo;
      
};
#endif
