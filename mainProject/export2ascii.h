#ifndef EXPORT2ASCII_H_PROTECTOR
#define EXPORT2ASCII_H_PROTECTOR

#include <iostream>
#include "exporteralgorithm.h"
using namespace std;


// ---------------------------------------------------
// file:	export2ascii.h
// author:	Frode Klevstul (frode@klevstul.com)
// started:	20030607
// ---------------------------------------------------

class Export2ASCII : public ExporterAlgorithm
{
    public:

		Export2ASCII();

		virtual ~Export2ASCII();
	
		string parseContent(string content, int pagewidth, int pageheight, bool showoutput) const;

		bool createOutputfile(string filename, string content) const;

	private:
	
		string substituteTags(string content) const;

		string getLine(string& content, int columnwidth) const;

};


#endif
