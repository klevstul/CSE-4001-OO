#include<iostream>
#include<string>
#include<sstream>
#include <stdio.h>
#include "lastpagetemp.h"

using namespace	std;


// ---------------------------------------------------
// file:	lastpagetemp.cpp
// author:	Frode Klevstul (frode@klevstul.com)
// started:	20030611
// ---------------------------------------------------


Lastpagetemp::Lastpagetemp(){

	// define importance for this template
	_importance[1]=false;
	_importance[2]=false;
	_importance[3]=false;
	_importance[4]=false;
	_importance[5]=false;
	_importance[6]=false;
	_importance[7]=false;
	_importance[8]=false;
	_importance[9]=false;
	_importance[10]=true;

	// define the number of	columns for this template
	_nocolumns = 1;
}


Lastpagetemp::~Lastpagetemp(){}



string Lastpagetemp::fetchArticles(int pagewidth, int pageheight, int pageno, Articlehandler* articlehandler) const{

	return fetchArticlesMethod(pagewidth, pageheight, pageno, _nocolumns, _importance, articlehandler,0);

}
