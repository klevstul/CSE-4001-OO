#ifndef EXPORTER_H_PROTECTOR
#define EXPORTER_H_PROTECTOR

using namespace std;

// ---------------------------------------------------
// file:	exporter.h
// author:	Frode Klevstul (frode@klevstul.com)
// started:	20030606
// ---------------------------------------------------

class Exporter
{	
    public:

		Exporter();

		~Exporter();
		
		string listFormats();
		
		string exportNewspaper(int choice, string filename, int pagewidth, int pageheight, string content);

	private:
	
		bool generateOutput();

		string _content;
		int _pagewidth;
		int _pageheight;
		int _format;
		string _filename;
		

};
#endif
