#include <iostream>
#include "page.h"
using namespace std;
#include<string>

Page::Page()
{
}

Page::Page(int pageno, int wdth, int high)
{
	pageNumber = pageno+1;
	width = wdth;
	height = high;
	pageContent = "";

}

Page::~Page()
{

}


string Page::getContent(Articlehandler* ahObj, int autobuild)
{
	pageContent = "";
	if (pageNumber == 1)
	{
		Template* templ;	
		templ = Template::getTemplate("FPAGTEMP-0");

		pageContent = templ->fetchArticles(width,height,pageNumber,ahObj);
	}

	else
	{
		Template* templ;	
		templ = Template::getTemplate("CONTTEMP");

		pageContent = templ->fetchArticles(width,height,pageNumber,ahObj);
	}

	return pageContent;
}

string Page::getContent(Articlehandler* ahObj)
{
	pageContent = "";

	char choice;
	string str;
	int bol;

	do
	{	
		bol = 0;

		cout << "Available templates:\n(1) Frontpage Template\n(2) Lastpage Template\n(3) Continue Template\n(4) Make your own template" << endl;
		cout << "Your choice: ";
		cin >> choice;
	
		if (choice=='1'){
			if (pageNumber==1){
				cout << "Specify max length on frontpage articles (0 for no max length): ";
				cin >> str;
				str = "FPAGTEMP-"+str;
			} else {
				str = "FPAGTEMP-0";
			}
		} else if (choice=='2')
			str = "LASTTEMP";
		else if (choice=='3')
			str = "CONTTEMP";
		else if (choice=='4')
		{
			str = "CUSTTEMP";
			cout << "Specify the importances for this template (ex: 1,2,5): ";
			string importances;
			cin >> importances;
			string columns;		
			cout << "Specify the number of columns for this template (max 10): ";
			cin >> columns;
			str= str + "-I:" + importances + "C:" + columns;
		}

		else
		{
			cout<<"Invalid input! Type again\n";
			bol = 1;
		}

	} while (bol);

	Template* templ;	
	templ = Template::getTemplate(str);

	pageContent = templ->fetchArticles(width,height,pageNumber,ahObj);

	return pageContent;
}





/*
	int select = 0;
	cout<<"choose a template for page #"<<pageNumber<<":\n";	
	objTemplate->listTemplate();

	int bol = 0;
	do
	{	
		cout<<"Your choice: ";
		cin>>select;
		if ((select<1) || (select>4))
		{
			cout<<"Invalid input! Type 1/2/3/4\n";
			bol = 1;
		}
	} while (bol);

	cout<<"Building Page #"<<pageNumber<<"...\n";
	pageContent = objTemplate->getTemplate(select, width, height);

	return pageContent;
}
*/




/*

	// my older version of getContent

	if (pageNumber == 1)
	{
		str = "FRONTPAGETEMP";
		bol = 0;
	}

	while (bol)
		if (pageNumber != 1)
		{
			cout<<"Choose a template for page "<<pageNumber<<"\n";
			cout<<"(1) - Custom   (importance: x)\n";
			cout<<"(2) - Continue (importance: all)\n";
			cout<<"(3) - LastPage (importance: 10)\n";
			cout<<"Your choice: ";
			cin>>choice;
			
			if (choice == '1')
			{
				str = "CUSTOMPAGETEMP";
				bol = 0;
			}
			
			else if (choice == '2')
			{
				str = "CONTPAGETEMP";
				bol = 0;
			}
			
			else if (choice == '3')
			{
				str = "LASTPAGETEMP";
				bol = 0;
			}
			
			else cout<<"Invalid choice! Try Again\n";
	}
	Template* temp1;
	temp1 = Template::getTemplate(str);		// line 48

	pageContent = temp1->fetchArticles(width, height, pageNumber);
*/

