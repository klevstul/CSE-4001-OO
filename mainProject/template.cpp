#include<iostream>
#include<string>
#include<sstream>
#include <stdio.h>
#include <string>
using namespace	std;
#include "template.h"
#include "frontpagetemp.h"
#include "lastpagetemp.h"
#include "contpagetemp.h"
#include "custtemp.h"
#include "articlehandler.h"

// ---------------------------------------------------
// file:	template.cpp
// author:	Frode Klevstul (frode@klevstul.com)
// started:	20030609
// ---------------------------------------------------

Template* Template::getTemplate(const string& name)
{
	if (name.substr(0,8) == "FPAGTEMP"){
		string str = name;
		string msize;
		const char* c_tmp;
		int maxsize;
		
		msize = str.substr(9,str.length());
		c_tmp = msize.c_str();
		maxsize = atoi(c_tmp);
		
		Template* templ = new Frontpagetemp(maxsize);
		return templ;
	} else if (name == "LASTTEMP") {
		Template* templ = new Lastpagetemp();
		return templ;
	} else if (name == "CONTTEMP") {
		Template* templ = new Contpagetemp();
		return templ;
	} else if (name.substr(0,8) == "CUSTTEMP") {
		string str = name;
		int i;
		bool importances [10];
		int nocol;
		int imp;
		string nocolumns;
		string importance;
		const char* c_tmp;


		for (i=0; i<11; i++)
			importances[i]=false;
		
		i = str.find("I:");
		str = str.substr(i+2, str.length());
		
		i = str.find("C:");
		nocolumns = str.substr(i+2,str.length());
		c_tmp = nocolumns.c_str();
		nocol = atoi(c_tmp);
		
		str = str.substr(0,i);

		//cout << "DEBUG: " << str << " nocol: " << nocol << endl;
		
		// set the importance bool array
		while (str.length()>0){
			i = str.find(",");
			if (i<0){
				importance = str;
				str = "";
			} else {
				importance = str.substr(0,i);
				str = str.substr(i+1,str.length());
			}
			c_tmp = importance.c_str();
			imp = atoi(c_tmp);
			importances[imp]=true;
		}
		
		Template* templ = new Custtemp(importances, nocol);
		return templ;
	}

	return NULL;
}



string Template::fetchArticlesMethod(int pagewidth, int pageheight, int pageno, int nocolumns, const bool importance [10], Articlehandler* articlehandler, int maxsize) const{

	Article* article;												// the article object

	int	maxbytesprcol = (pagewidth	* pageheight) /	nocolumns;		// bytes per column
	int maxbytesprline = maxbytesprcol / pageheight; 				// bytes per line
	int maxbytesprpage = pagewidth	* pageheight;					// bytes per page
	int picsize = maxbytesprline * 5;								// a pic is defined taking five lines
	int	bytescol =	0; 												// bytes left in column
	int	bytespage = 0; 												// bytes left in page
	int	articleno;													// articlenumber as an int
	int	columns_done = 0; 											// how many columns that's finished/filled up
	int needbytes = 0; 												// how many bytes is needed to start printing an article out
	int bytesprinted = 0; 											// how many bytes of the content/body that is printed
	int previousartno = 0;											// keep track of the previous article we printed out
	int contwithart = -1;											// keep track of which article we shall continue in next column
	int tmp;														// helpcounter
	int	q;															// helpcounter
	int maxsizetoprint = maxsize; 									// max bytesize of articles in page 1 to be written out (0 to disable max size)
	bool maxsizedcontinuingart = false;								// if a max size'ed article is continuing...

	string output;													// the output to be returned from this method
	string artno;													// the articlenumber as a string
	string pno;														// page number
	string heading;													// the heading/title
	string pic;														// the picture
	string content;													// the content/body of a newspaper
	string tmpcontent; 												// temporar string used to "stuff" bytes in the column
	string continuing;												// continuing tag
	
	// convert int "pageno" into string "pno"
	stringstream p;
	p << pageno;
	pno = p.str();

	// update how many bytes there are left in the page
	bytespage = maxbytesprpage;
	
	// start the page
	output = "[page#"+pno+"]";
	

	// -----------------------------------
	// go though untill the page is full
	// -----------------------------------
	while (bytespage>0){

		// start a new column
		output+="[column]";

		// for every column we update the bytes left in column, to column size
		// this number is decremented as we put articles into the column
		bytescol = maxbytesprcol;

		// -----------------------------------------------
		// go through all the articles for every column
		// we add articles if they fit in the column (and if they have right importance and bytes left)
		// ------------------------------------------------
		q=0;
		while ( q<articlehandler->getSize() ){

			// we might have to continue with an article from last column
			// then change the array-counter
			if (contwithart>=0){
				q = contwithart;
				contwithart = -2;
			// the array-counter was changed last round, so start looking for
			// articles from the start of the array again
			} else if (contwithart == -2) {
				q = 0;
				contwithart = -1;
			}
			
			article	= articlehandler->articles[q];

			// -----------------------------------------------------------------
			// In case there is a maxsize on articles, and we are on the frontpage
			// we only want to print out a part of the article. In that case 
			// we only want to start with new articles.
			// -----------------------------------------------------------------
			if (maxsizetoprint && pageno==1){
				// the article is not new, so jump to next article
				if ( (*article).getBytes()!=(*article).getSize()){
					q++;
					continue;
				}
			}

			// ------------------------------------------------------------------------------------------
			// check if	this article has the right importance for this template, and has more bytes left
			// ------------------------------------------------------------------------------------------
			if(	importance[(*article).getImportance()]	&& (*article).getBytes()>0 ){

				// convert articlenumber into a string
				articleno = q+1;
				stringstream s0;
				s0 << articleno;
				artno = s0.str();

				// convert char* into string (heading)
				stringstream s1;
				s1 << (*article).getHeading();
				heading	= s1.str();
				
				// picture
				stringstream s2;
				s2 << (*article).getPicture();
				pic = s2.str();

				// content/body
				stringstream s3;
				s3 << (*article).getContent();
				content = s3.str();

				// -----------------------------------------------------------------
				// calculate how many bytes we need to start printing this article
				// -----------------------------------------------------------------
				needbytes = 0;
				if (pic!=""){
					needbytes = picsize;
				}
				needbytes += heading.length();
				// need at least two lines extra after heading and pic
				needbytes += maxbytesprline * 4;
				
				// calculate how many bytes of this article that are written from before
				bytesprinted = (*article).getSize() - (*article).getBytes();

				// if the article wont finished and it is the last column, 
				// we also need two lines of space printing out the continuing tag
				if ( content.substr(bytesprinted,content.length()).length()>bytescol && (previousartno!=articleno) ){
					needbytes += maxbytesprline * 2;
				}

				// ------------------------------------------------------------
				// if we have space to start an article in this column
				// ------------------------------------------------------------
				if (needbytes <= bytescol){

					// we have enough space to start this article, so start printing it by adding the article tag
					output+="[article#"+artno+"]";

					// -----------------------------------------
					// check if this article is continued...
					// -----------------------------------------
					if ( (*article).getBytes() != (*article).getSize() ){
						// check if article is continued from another page
						// or just continued from previous column
						if ( previousartno != articleno ){
							// the article is continued from another page,
							// so we print out the heading and continuing tag

							// calculate how many lines the heading fills up
							tmp = heading.length()/maxbytesprline;
							tmp++;
							
							// subtract the headingspace from bytes left in column
							bytescol -= tmp*maxbytesprline;

							// append heading and continuing tag to output string
							output+="[continued#-"+artno+"][br][ctitle]"+heading+"[/ctitle][br]";
							
							// subtract the space for the newlines
							bytescol -= maxbytesprline*2;
						}
					// -------------------------------------------------------------------
					// if not continuing, it's new, so print out heading, picture etc...
					// -------------------------------------------------------------------
					} else {

						// print title/heading
						output+="[title]"+heading+"[/title][br]";
						
						// calculate how many lines the heading fills up
						tmp = heading.length()/maxbytesprline;
						tmp++;
						
						// subtract the headingspace from bytes left in column
						bytescol -= tmp*maxbytesprline;
						
						// picture
						if (pic!=""){
							output+="[pic]"+pic+"[/pic]";
							bytescol -= picsize;
						}

						// a newline after heading (and picture)
						output+="[br]";
						bytescol -= maxbytesprline;
					}
					
					//start the body
					output+="[body]";

					
					// --------------------------------------------------------------
					// if there is a max size to print and pagnumber is 1 (frontpage), 
					// we manipulate the content so it fits this max size
					// --------------------------------------------------------------
					if (maxsizetoprint && pageno==1){
						if ( content.length() > maxsizetoprint ){
							content = content.substr(0,content.substr(0,maxsizetoprint).rfind(" "));
							maxsizedcontinuingart = true;
						}
					}


					// ------------------------------------------------------------
					// we have space for only a part of the article in the column
					// ------------------------------------------------------------
					if ( ((content.length()-bytesprinted) + (maxbytesprline*2)) > bytescol){

						// check if article is continuing on another page...
						if ( ((columns_done+1)==nocolumns && (*article).getBytes()>0) ){
							continuing="[br][continuing#-"+artno+"]";
							// remove two lines of space, since this this takes at least one line and at maximum two lines
							bytescol-=maxbytesprline*2;
						} else {
							// store that we'll continue with this article in the next column
							contwithart = q;
							// we don't bother to check for anymore articles that fit in this column,
							// so create a jump to the next column
							q = articlehandler->getSize();
						}

						// prevent that a word is devided into two at the end of a column by finding the last space
						tmp = content.substr(bytesprinted, bytescol).rfind(" ");
						tmp = tmp - bytesprinted + 1;
						
						// prepare the text for the column
						tmpcontent = content.substr(bytesprinted, tmp);

						// append the text to the column
						output+=tmpcontent;

						// update bytes left in the article
						(*article).setBytes( (*article).getBytes()-tmpcontent.length() );
						
						// the column is full, so there are no bytes left
						bytescol = 0;

					// ----------------------------------------------------
					// we have space for the entire article, and two newlines, in the column
					// ----------------------------------------------------
					} else {
						// prepare the text to be written
						tmpcontent = content.substr(bytesprinted,content.length());
						
						// add two newlines after the body
						output+=tmpcontent;
						
						// the article is finished, so there are no more bytes to write
						if (maxsizedcontinuingart)
							(*article).setBytes( (*article).getBytes()-tmpcontent.length() );
						else
							(*article).setBytes(0);
						
						// update the number of bytes left in the column
						bytescol -= (tmpcontent.length()+(maxbytesprline*2));
						
						// ------------------------------------------------------------------------
						// continuing-tag if this actually is a partial article on the frontpage,
						// otherwise we do a double newline before the next article
						// ------------------------------------------------------------------------
						if (maxsizedcontinuingart){
							continuing="[continuing#-"+artno+"][br][br]";
							bytescol-=maxbytesprline*2;
							maxsizedcontinuingart = false;
						} else {
							continuing="[br][br]";
						}
						
					}

					// add continuing tag (the string might be empty)
					output+=continuing;

					//end the body
					output+="[/body]";
					
					// end article tag
					output+="[/article]";

					// save this articlenumber for next round (to prevent printing out cont. tag for this)
					previousartno = articleno;
				}
				
//				cout << "HEADING: \n" << heading << endl;
//				cout << "NEED BYTES:: " << needbytes << endl;
				
			}
			// increas the q/articlecounter
			q++;
		} // while q<size
		
		// we have done one more column
		columns_done++;
		
		// end the column-tag
		output+="[/column]";

		// if we have reached the total number of column, there are no bytes left in the page
		if (columns_done==nocolumns)
			bytespage=0;
		
	}
	// end the page-tag
	output+= "[/page]";


	
//	cout <<	"DEBUG:	pageinfo - width: "	<< pagewidth <<	" height: "	<< pageheight << " bytes pr col: " << maxbytesprcol <<	endl;
	
	
	return output;
}

