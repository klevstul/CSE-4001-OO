#include "monoalpha.h"

// --------------------------------------------------
// File: 	monoalpha.cpp
// Author:	Frode Klevstul	(frode@klevstul.com)
// Started:	28.03 2003
// --------------------------------------------------




Monoalpha::Monoalpha(const string& key)
{
	if ( !validKey(key) )
		throw InvalidKeyException();

	int i;
	_key = key;

	for (i = 0; _key[i]!=0; i++)						// converts key to uppercase
		_key[i] = toupper(_key[i]);
}




Monoalpha::~Monoalpha()
{
}




string Monoalpha::encrypt(const string& m) const
{
	string encrypted_str = m;
	int i;
	int dec;

	for (i = 0; encrypted_str[i]!=0; i++)
	{
		encrypted_str[i] = toupper(encrypted_str[i]);	// uppercase
		if ( (int)encrypted_str[i] == 32 )				// changes 'space' (32) to '[' (91) -> 91-65 = slot 26
			(int)encrypted_str[i] = 91;
														// example: encrypted_str[i] = 'A'	key = 'EFGH'
		dec = (int)encrypted_str[i]-65;					// example: 'A' has dec value 65 -> dec = 0
		(int)encrypted_str[i] = (int)_key[dec];			// example: 'A' is changed to key[0] = 'E'

	}

	return encrypted_str;
}




string Monoalpha::decrypt(const string &m) const
{
	string decrypted_str = m;
	int i;
	int dec;

	for (i = 0; decrypted_str[i]!=0; i++)
	{
		decrypted_str[i] = toupper(decrypted_str[i]);

		dec = _key.find(decrypted_str[i]);				// returns the position of the given character in the string

		if (dec == 26)
			(int)decrypted_str[i] = 32;					// space
		else
			(int)decrypted_str[i] = dec + 97;
	}

	return decrypted_str;
}




bool Monoalpha::validKey(const string& key)
{
	int i=0;
	string s = "";									// used for finding duplicate characters
	string c;
	int y;

	for (i=0; key[i]!=0; i++)						// goes through the string a checks if all characters lowercase letters or space
	{		
		if ( ((int)key[i]!=32 && (int)key[i]<65) || ((int)key[i]>90 && (int)key[i]<97) || (int)key[i]>122 )
			return false;

		y = s.find(key[i]);							// checks if all the characters are unique
		if (y>-1&&y<26)								// if this is true, we have already read this character
		{
			return false;							// so we return false : key is invalid, a char used more than once
		}
		else
		{
			c=key[i];
			s.insert(i,c);							// else: store character into string s
		}
	}

	if (i!=27)										// the key must be exactly 27 char long
		return false;

	return true;
}
