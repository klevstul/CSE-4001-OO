#include <iostream>
#include "crypto.h"
#include "exception.h"

using namespace std;

int main()
{
    
    Crypto* trans;
    Crypto* trans2;
    Crypto* trans3;
    Crypto* trans4;

    string org_msg;
    string enc_msg;
    string dec_msg;

    // - - - - - -
    // Ceasar
    // - - - - - -

    try
    {
        trans = Crypto::getCrypto("caesar", "5");
    }
    catch(InvalidKeyException e)
    {
        exit(-1);
    }

    org_msg = "Pull the brown book on the top shelf to activate";
    enc_msg = trans->encrypt(org_msg);
    dec_msg = trans->decrypt(enc_msg);

    cout << "Caesar: original message: \"" << org_msg << "\"" << endl;
    cout << "Caesar: encoded message: \"" << enc_msg << "\"" << endl;
    cout << "Caesar: decoded message: \"" << dec_msg << "\"" << endl << endl;

    delete trans;
    trans = 0;          // sets pointer to zero to avoid potential problems with dangling pointers

    // - - - - - -
    // Monoalpha
    // - - - - - -

    try
    {
        trans2 = Crypto::getCrypto("monoalpha", "QA Zwsxedcrfvtgbyhnujmikolp");
    }
    catch(InvalidKeyException e)
    {
        exit(-1);
    }

    org_msg = "Pull the brown book on the top shelf to activate";
    enc_msg = trans2->encrypt(org_msg);
    dec_msg = trans2->decrypt(enc_msg);

    cout << "Monoalpha: original message: \"" << org_msg << "\"" << endl;
    cout << "Monoalpha: encoded message: \"" << enc_msg << "\"" << endl;
    cout << "Monoalpha: decoded message: \"" << dec_msg << "\"" << endl << endl;

    delete trans2;
    trans2 = 0;

    // - - - - - -
    // Transposition
    // - - - - - -

    try
    {
        trans3 = Crypto::getCrypto("transposition", "240153");
    }
    catch(InvalidKeyException e)
    {
        exit(-1);
    }


    org_msg = "The password for today is deceptive";
    enc_msg = trans3->encrypt(org_msg);
    dec_msg = trans3->decrypt(enc_msg);

    cout << "Transposition: original message: \"" << org_msg << "\"" << endl;
    cout << "Transposition: encoded message: \"" << enc_msg << "\"" << endl;
    cout << "Transposition: decoded message: \"" << dec_msg << "\"" << endl << endl;

    string s1 = "test transposition with same object";
    string s2 = trans3->encrypt(s1);
    string s3 = trans3->decrypt(s2);

    cout << "Transposition: original message: \"" << s1 << "\"" << endl;
    cout << "Transposition: encoded message: \"" << s2 << "\"" << endl;
    cout << "Transposition: decoded message: \"" << s3 << "\"" << endl << endl;


    delete trans3;
    trans3 = 0;

    // - - - - - - - - 
    // Cencryption 
    // - - - - - - - -
    try
    {
        trans4 = Crypto::getCrypto("cencryption", "caesar,16,50;transposition,30142,50;caesar,21,30;");
    }
    catch(InvalidKeyException e)
    {
        exit(-1);
    }

    org_msg = "In this assignment you will develop a simple cryptographic library that combines three different encryption algorithms  The encryption algorithms are very old an well known and are not the strongest encryption schemes available today but are much more interesting to program  The algorithms can be used together to encrypt different parts of a single text message";
    enc_msg = trans4->encrypt(org_msg);
    dec_msg = trans4->decrypt(enc_msg);

    cout << "Cencryption: original message: \"" << org_msg << "\"" << endl;
    cout << "Cencryption: encoded message: \"" << enc_msg << "\"" << endl;
    cout << "Cencryption: decoded message: \"" << dec_msg << "\"" << endl << endl;

    delete trans4;
    trans4 = 0;


    return 0;
}

