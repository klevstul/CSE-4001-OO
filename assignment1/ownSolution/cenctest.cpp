#include <iostream>
#include <string>
#include "crypto.h"
#include "exception.h"

using namespace std;

int main()
{
    Crypto* trans;

    try
    {
        trans = Crypto::getCrypto("cencryption", "caesar,16,50;transposition,30142,50;caesar,21,30;");
    }
    catch (InvalidKeyException e)
    {
        cout << "OK - Exception caught in centest.cpp" << endl;
        exit(-10);
    }

    string plaintext = "In this assignment you will develop a simple cryptographic library that combines three different encryption algorithms  The encryption algorithms are very old an well known and are not the strongest encryption schemes available today but are much more interesting to program  The algorithms can be used together to encrypt different parts of a single text message";
    string ciphertext = trans->encrypt(plaintext);

    string message = trans->decrypt(ciphertext);

    delete trans;

    cout << "original message: '" << plaintext << "'" << endl;
    cout << "cyphertext      : '" << ciphertext << "'" << endl;
    cout << "message         : '" << message<< "'" << endl;

    return 0;
}

/*
 * Should print:
original message: 'In this assignment you will develop a simple cryptographic library that combines three different encryption algorithms  The encryption algorithms are very old an well known and are not the strongest encryption schemes available today but are much more interesting to program  The algorithms can be used together to encrypt different parts of a single text message'
cyphertext      : 'YCPIXYHPQHHYWCBUCIPNDJPLYAAPTUKUADEPQPHYBEAUPSGNEIITCHLEH  RB RDRFIEAI  CEMCTTONGIRNPSOBAE ETRANYFHELSJNCIHUVFAILCNBGMUUNBZUZHXLSJIYDCPQAWDGYIXBHPQGUPKUGNPDATPQCPLUAAP CDLCPQCTPQGUHCEHSMTS ERSO GVTENACER PONLYTNIOO   TTNSITLEBEA AYVSUWONUVLZUGOXBUGILZUCHNZLZMNYCWPIDPEGDWGQBPPIXUPQAWDGYIXBHPSQCPRUPJHUTPIDWUIXUN CAYSEFR TN GIEPIDLTM EASN PS TTS ORRO FTEEETF RXVAZ'
message         : 'in this assignment you will develop a simple cryptographic library that combines three different encryption algorithms  the encryption algorithms are very old an well known and are not the strongest encryption schemes available today but are much more interesting to program  the algorithms can be used together to encrypt different parts of a single text message'
*/

