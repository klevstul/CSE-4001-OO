#include <string>
#include <iostream>
#include "crypto.h"
#include "caesar.h"
#include "monoalpha.h"
#include "transposition.h"
#include "cencryption.h"

using namespace std;

// --------------------------------------------------
// File:    crypto.cpp
// Author:  Frode Klevstul  (frode@klevstul.com)
// Started: 26.03 2003
// --------------------------------------------------




Crypto* Crypto::getCrypto(const string& name, const string& key)
{
    // creates a new object of given type/name
    if (name == "caesar")
    {
        Caesar *pCrypto = new Caesar(key);
        return pCrypto;
    } 
    else if (name == "monoalpha")
    {
        Monoalpha *pCrypto = new Monoalpha(key);
        return pCrypto;
    } 
    else if (name == "transposition")
    {
        Transposition *pCrypto = new Transposition(key);
        return pCrypto;
    } 
    else if (name == "cencryption")
    {
        CEncryption *pCrypto = new CEncryption(key);
        return pCrypto;
    } 
    
    return NULL;
}
