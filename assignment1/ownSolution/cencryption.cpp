#include "cencryption.h"

// --------------------------------------------------
// File:    cencryption.cpp
// Author:  Frode Klevstul  (frode@klevstul.com)
// Started: 03.04 2003
// --------------------------------------------------


/*
 *  ------------------------------------------------------
 *  Comments on this solution of the CEncryption class:
 *  ------------------------------------------------------
 *
 *  WHY MULTICRYPTION IN CONSTRUCTOR?
 *  ----------------------------------
 *  When I first made this class I didn't see that it was the
 *  constructor who should trow the invalid key exception. 
 *  That's why I here first encrypt a temporary string, to check
 *  if the ceencryption key is OK. If it works then it will
 *  work later as well (we will not get any invalidKey except
 *  during multiCryption in "encrypt()" and "decrypt()" methods.
 *  This is a BAD and INEFFICIENT way to do it. What I should 
 *  have done is to create all the object in the constructor
 *  and re-used the same objects during en-/decryption.
 *  Right now I am creating and deleting a lots of object, 
 *  first testing the key, then everytime I encrypt or decrypt.
 *
*/


CEncryption::CEncryption(const string& key)
{
    _key = key;
    string tmp = multiCryption("testing CEncryption keys", "encrypt");
}




CEncryption::~CEncryption()
{
}




string CEncryption::encrypt(const string& m) const
{
    string enc_str = multiCryption(m, "encrypt");
    return enc_str;
}




string CEncryption::decrypt(const string& m) const
{
    string enc_str = multiCryption(m, "decrypt");
    return enc_str;
}




string CEncryption::multiCryption(const string& str, const string& type) const
{
    Crypto* trans;                      // a Crypto pointer
    string s;                           // a help string used to store substrings of key
    string ret_str;                     // the complete crypted (encrypted/decrypted) string to be returned
    string crypto_method = "undef";     // the crypto method to be used
    string crypto_key = "undef";        // the key for cryptation
    string bytes;                       // a help string to select the number of bytes to be crypted with a spesific key
    string crytpo_ret;                  // used to store the return string from the crypt-action
    string crypt_str;                   // used to store a substring of the original string, that we will crypt (encrypt/decrypt)
    int str_length = str.length();      // the length of the string we are going to crypt
    int bytes_crypting = 0;             // how many bytes we are going to crypt next time
    int bytes_crypted = 0;              // how many bytes we have crypted
    int y;                              // help counters in for loop, used when going through strings
    unsigned int i;                     // help counter, unsigned because of string.length() comparison
    int key_pos = -1;                   // used for storing position when searching in string
    int keysubstr_pos = -1;             // used for storing position when searching in string


    while (bytes_crypted < str_length){                                 // have to do this until all bytes are crypted
                                                                        // split up key, example: "caesar,16,50;transposition,30142,50;caesar,21,30;" 
        for (i=0; _key[i]!=0; i++)                                      // go through the key
        {
            if (_key[i]==';')                                           // first we split the key on ';'
            {
                s = _key.substr(key_pos+1, i-key_pos-1);                // stores substring in 's' example: "caesar,16,50"
                key_pos = i;                                            // save this position in last_position for next round
                for (y=0; s[y]; y++)                                    // go through the substring
                {
                    if (s[y]==',')                                      // the elements in this string is separated by ','
                    {
                        if (crypto_method == "undef")
                            crypto_method = s.substr(keysubstr_pos+1, y-keysubstr_pos-1);
                        else if (crypto_key == "undef")
                            crypto_key = s.substr(keysubstr_pos+1, y-keysubstr_pos-1);
                        keysubstr_pos = y;                              // stores the position for next time in the loop
                    }
                }
    
                bytes = s.substr(keysubstr_pos+1,y);                    // how many bytes we are going to crytp
                bytes_crypting = atoi(bytes.c_str());                   // converts the number given as a string into int

                trans = Crypto::getCrypto(crypto_method,crypto_key);    // creates a new crypto object of given type with given key
                crypt_str = str.substr(bytes_crypted,bytes_crypting);   // gets the string we will crypt    

                if (type == "encrypt")
                    crytpo_ret = trans->encrypt(crypt_str);             // we encrypt this string
                else if (type == "decrypt")         
                    crytpo_ret = trans->decrypt(crypt_str);             // we decrypt this string   
                delete trans;                                           // delete the crypto object
    
                ret_str.insert(bytes_crypted,crytpo_ret);               // we insert the string crypted in return string
                bytes_crypted += bytes_crypting;                        // stores how many bytes of the string we have crypted

                crypto_method = "undef";                                // resets values for next round
                crypto_key = "undef";
                keysubstr_pos = -1;
            }
            
            if (bytes_crypted > str_length)                             // we can have finished crypting the string
                return ret_str;                                         // before we have stepped through the entire key


        } // end of for loop
        key_pos = -1;                                                   // resets position of key for next round
    } // end of while loop
    return ret_str;
}
