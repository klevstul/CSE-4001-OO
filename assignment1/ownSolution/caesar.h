#ifndef CAESAR_H_PROTECTOR
#define CAESAR_H_PROTECTOR

#include <string>
#include <iostream>
#include "crypto.h"
#include "exception.h"

using namespace std;

// --------------------------------------------------
// File:    caesar.h
// Author:  Frode Klevstul  (frode@klevstul.com)
// Started: 27.03 2003
// --------------------------------------------------


class Caesar : public Crypto
{
    public:
        /**
         * Constructor
         * @param key the key used for encryption/decryption
         */
        Caesar(const string& key);

        /**
         * Destructor
         * made virtual since parent class has virtual functions
         */
        virtual ~Caesar();

        /**
         * Encrypts a text
         * @param m the string that is going to be encrypted
         * @return the encrypted text
         */
        string Caesar::encrypt(const string& m) const;

        /**
         * Decrypts a text
         * @param m the string that is going to be decrypted
         * @return the decrypted text
         */
        string Caesar::decrypt(const string& m) const;

    private:
        /**
         * Checks if a key is valid
         * @param key the key used for encoding/decoding
         * @return true: valid false: invalid
         */
        bool Caesar::validKey(const string& key);

        /**
         * The key used for encryption/decryption
         */
        int _key;

};




#endif
