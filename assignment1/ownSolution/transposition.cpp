#include "transposition.h"

// --------------------------------------------------
// File:    transposition.cpp
// Author:  Frode Klevstul  (frode@klevstul.com)
// Started: 31.03 2003
// --------------------------------------------------


/*
 *  ------------------------------------------------------
 *  Comments on this solution of the Transposition class:
 *  ------------------------------------------------------
 *
 *  WHY A CHAR[100][10] MATRIX?
 *  ----------------------------
 *  I do see the problem as defining a matrix of 100x10 (max 1000 characters).
 *  When I first did this I misunderstood the assignment text and thought I could
 *  expect a matrix of maximum 10x10 (100 characters). I should have implemented 
 *  this using vectors.
 *
 *  WHY CREATION OF NEW OBJECTS IN EN-/DECRYPTION?
 *  -----------------------------------------------
 *  This is a inefficient solution. Everytime en-/decryption is called it's created
 *  a new class, and the different operations are done in this class (shuffeling the
 *  columns around). In stead of doing it this way I could have used the methods to 
 *  return strings between themself. That way I could also have some of the members
 *  in this class removed.
 *
 *  THE CONSTRUCTOR
 *  ----------------
 *  I should also have done much more in the constructor. This goes for the entire 
 *  solution (not only this class). That way the en-/decyrption methods would have 
 *  been cleaner and more efficient.
 *
*/



Transposition::Transposition(const string& key)
{
    if ( !validKey(key) )
        throw InvalidKeyException();

    _key = key;
    _cols = key.length();
    _rows = 0;
}




Transposition::~Transposition(){}




string Transposition::encrypt(const string& m) const
{
    string encrypt_str;
                                                        // since this method is const, we cannot change the state of this object
                                                        // so we have to make a new object of same type (otherwise it would have
    Transposition* trans;                               // been better to make the changes directly in this object)
    trans = new Transposition(_key);                    // step 1: create a new transposition class with ecryption key
    trans->initHorizontal(m);                           // step 2: initialize trans.matrix with string that is going to be encrypted
    trans->runKey();                                    // step 3: run the key on the matrix
    trans->initHorizontal(trans->getStringVertical());  // step 4: insert the new string (by reading col by col) in the matrix
    trans->runKey();                                    // step 5: run the key on this new matrix
    encrypt_str = trans->getStringVertical();           // step 6: get the final encrypted string
    delete trans;

    return encrypt_str;
}




string Transposition::decrypt(const string& m) const
{
    string decrypt_str;                                 // the encrypted string
    int i;                                              // help counter
    unsigned int ui;                                    // help counter (used for string length comparison, has to be unsigned)
    int pos;                                            // position in string, used for generating dec_key
    string dec_key;                                     // decryption key
    string s;                                           // help string used for temporary storage
    string org_key = _key;                              // stores the original key

    // Generates the key used for decryption. This is done by finding the different numbers's position in original key:
    // key: 240153
    // dec key: 0.pos in original key = 2, 1.pos = 3, 2.pos = 0 etc.
    // ==> 230514 is used for decryption
    for(ui=0; ui<_key.length(); ui++)
    {
        pos = _key.find((char)(ui+48));
        s = (char)(pos+48);
        dec_key.insert(ui,s);
    }

    Transposition* trans;
    trans = new Transposition(dec_key);             // step 1: create a new transposition class with decryption key
    trans->initVertical(m);                         // step 2: initialize the matrix verticaly with encrypted string
    trans->runKey();                                // step 3: run decryption key
    decrypt_str = trans->getStringHorizontal();     // step 4: read the string horizontaly
    trans->initVertical(decrypt_str);               // step 5: init the matrix verticaly with new string
    trans->runKey();                                // step 6: run the decryption key on the new matrix
    decrypt_str = trans->getStringHorizontal();     // step 7: read the string horizontaly
    delete trans;

    for(i=0; decrypt_str[i]!=0; i++)                // makes the result lowercase
        decrypt_str[i] = tolower(decrypt_str[i]);

    return decrypt_str;
}




void Transposition::initHorizontal(const string& str)
{   initialize(str, "horizontal");
}




void Transposition::initVertical(const string& str)
{   initialize(str, "vertical");
}




void Transposition::initialize(const string& str, const string& type)
{
    int str_length = str.length();
    int c=0;

    int x_max;
    int y_max;
    int x;
    int y;
    int* row;
    int* col;

    _rows = int(str_length/_cols);                  // rounds up/down to the nearest integer
    if ( (float)str_length/_cols > _rows)           // adds 1 if the result is rounded down
        _rows++;

    if (type == "horizontal")                       // horizontal initialization:
    {                                               // go through row by row
        x_max = _rows;                              //      ***********
        y_max = _cols;                              //      *---------*
        col = &y;                                   //      *---------*
        row = &x;                                   //      *---------*
    }                                               //      ***********
    else if (type == "vertical")                    // vertical initialization
    {                                               // go through column by column
        x_max = _cols;                              //      *********** 
        y_max = _rows;                               //     * |  |  | * 
        col = &x;                                   //      * |  |  | * 
        row = &y;                                   //      * |  |  | * 
    }                                               //      *********** 


    for (x=0; x<x_max; x++)
    {
        for (y=0; y<y_max; y++)
        {
            if (c<str_length)
                _matrix[*row][*col] = str[c];
            else
                _matrix[*row][*col] = (char)32;     // adds space to complete the matrix
            c++;
        }
    }
}




void Transposition::runKey(){

    char new_matrix[def_matrix_rows][def_matrix_cols];          // temporary new matrix
    unsigned int ui;                                            // have to use unsigned into for comparison with string.length (returs unsigned)
    int i, y;                                                   // help counters, y for rows and i for columns
    int key_value;                                              // one digit at the time from the key is stored here
    string c;                                                   // used to store a char at a position as a string

    for (y=0; y<_rows; y++)                                     // goes through the entire matrix
    {                                                           // NB: _key[i]!=0 did not work here! Somehow the _key string had a lot of other
                                                                //     bytes after the key was finished, so _key[i]!= was never 0 !!!
        for (ui=0; ui<_key.length(); ui++)                      // key.length = cols - travels through all columns
        {
            c = _key[ui];
            key_value = atoi(c.c_str());
            new_matrix[y][key_value] = toupper(_matrix[y][ui]); // new position = key_value, old_position = i
        }
    }
    
    for (y=0; y<_rows; y++)                                     // "submit" the changes to the original matrix
        for (i=0; i<_cols; i++)
            _matrix[y][i] = new_matrix[y][i];

}




string Transposition::getStringVertical()
{   return getString("vertical");
}




string Transposition::getStringHorizontal()
{   return getString("horizontal");
}




string Transposition::getString(const string& type)
{
    string return_str;
    string s;
    int x_max;
    int y_max;
    int x;
    int y;
    int* row;
    int* col;
    int c=0;

    if (type == "horizontal")
    {
        x_max = _rows;
        y_max = _cols;
        col = &y;
        row = &x;
    }
    else if (type == "vertical")
    {
        x_max = _cols;
        y_max = _rows;
        col = &x;
        row = &y;
    }                               

    for (x=0; x<x_max; x++)
    {
        for (y=0; y<y_max; y++)
        {
            s = _matrix[*row][*col];
            return_str.insert(c,s);
            c++;
        }
    }

    return return_str;
}




void Transposition::print(){
    int row,col;

    for (row=0; row<_rows; row++)
        for (col=0; col<_cols; col++)
        {
            cout << "|" << _matrix[row][col] << "|";
            if (col==(_cols-1))
                cout << endl;
        }
}




bool Transposition::validKey(const string& key)
{
    string s_tmp;
    string str;
    string c;
    int max = 0;                                    // the biggest number in the key
    int y;
    int i;
                                                    // Note: it's ok to check "key[i]!=0" here (and not key.length()), 
                                                    // because this key is a constant string and will never change. 
                                                    // It will never have "dirty bytes" after the key ends.
    for (i=0; key[i]!=0; i++)                       // goes through the string a checks if all characters are numbers
    {
        if (!isdigit(key[i]))                       // if not all characters are digits
            return false;                           // it's a invalid key

        s_tmp = key[i];
        y = atoi(s_tmp.c_str());

        if (y>max)                                  // we store the biggest number in the key
            max=y;

        y = str.find(key[i]);                       // checks if all the characters/numbers are unique
        if (y>=0)                                   // if this is true, we have already read this character
        {
            return false;                           // so we return false : key is invalid
        }
        else
        {
            c=key[i];
            str.insert(i,c);                        // else: store character into string str
        }
    }

    for (i=0; i<max; i++)                           // goes throug the string and checks if we have all the numbers from 0..max
    {
        y = key.find((char)(i+48));
        if (y<0)                                    // if we don't find all the numbers from 0 .. max the key is invalid
            return false;
    }

    return true;
}
