#ifndef AGER_PROTECTOR
#define AGER_PROTECTOR

#include <iostream>
#include <iterator>
#include <list>

using namespace std;



template <typename T, typename CON = list<pair<size_t,T> > >
class Ager
{
	public:

		// ----------------
		// iterator class
		// ----------------
		class iterator{

			private:
				friend class Ager<T,CON>;
				typedef typename CON::iterator cLI;
				const Ager<T,CON> *pC;
				cLI i;
				size_t n;
				typedef int ptrdiff_t;
			
			public:
				// ---
				// constructors
				// ---
				iterator() : pC(0), n(0) { }
				iterator(const Ager<T,CON>* p_, cLI i_) : pC(p_), i(i_), n(0) { }

				// ---
				// operator overloading
				// ---
				iterator& operator++() {	// prefix
					if (pC)
						if (++n < pC->container.size())
							++i;
					else 
						pC = 0;
					return *this;
				}

				iterator& operator++ (int) {	// postfix
					//iterator tmp = *this;
					++*this;
					//return tmp;
					return *this;
				}

				operator const void* () { return pC; }

				//T& operator* () { return (T&)(*i); }
				T& operator* () { return i->second;}

				iterator& operator-- () { 	// prefix
					pC = pC->prev; 
					return *this;
				}

				iterator& operator-- (int) {	// postfix
					//iterator tmp = *this;
					--*this;
					//return tmp;
					return *this;
				}
				
				iterator& operator+ (ptrdiff_t step) {
					i += step;
					return *this;
				}

				iterator& operator- (ptrdiff_t step) {
					i -= step;
					return *this;
				}

				size_t operator- (iterator i_) {
					size_t diff = i_ - i;
					return diff;
				}

				T* operator-> () {
					return &(i->second);
				}

				T operator[](int index){
					i += index;
					return i->second;
				}

				// ---
				// comparators
				// ---
				bool operator!= (iterator i_){
					return pC != i_.pC;
				}

				bool operator== (iterator i_){
					return pC == i_.pC;
				}

				bool operator> (iterator i_){
					return pC > i_.pC;
				}

				bool operator< (iterator i_){
					return pC < i_.pC;
				}

				bool operator<= (iterator i_){
					return pC <= i_.pC;
				}

				bool operator>= (iterator i_){
					return pC >= i_.pC;
				}

				// ---
				// other methods
				// ---
				size_t getAge(){return i->first;};
		};





		// ----------------
		// reverse_iterator class
		// ----------------
		class reverse_iterator{

			private:
				friend class Ager<T,CON>;
				typedef typename CON::reverse_iterator cLI;			
				const Ager<T,CON> *pC;
				cLI i;
				size_t n;
				typedef int ptrdiff_t;
			
			public:
				// ---
				// constructors
				// ---
				reverse_iterator() : pC(0), n(0) { }
				reverse_iterator(const Ager<T,CON>* p_, cLI i_) : pC(p_), i(i_), n(0) { }

				// ---
				// operator overloading
				// ---
				reverse_iterator& operator++() {	// prefix
					if (pC)
						if (++n < pC->container.size())
							++i;
					else 
						pC = 0;
					return *this;
				}

				reverse_iterator& operator++ (int) {	// postfix
					//reverse_iterator tmp = *this;
					++*this;
					//return tmp;
					return *this;
				}

				operator const void*() { return pC; }

				//T& operator*() { return (T&)(*i); }
				T& operator* () { return i->second;}

				reverse_iterator& operator-- () { 	// prefix
					pC = pC->prev; 
					return *this;
				}

				reverse_iterator& operator-- (int) {	// postfix
					//reverse_iterator tmp = *this;
					--*this;
					//return tmp;
					return *this;
				}
				
				reverse_iterator& operator+ (ptrdiff_t step) {
					i += step;
					return *this;
				}

				reverse_iterator& operator- (ptrdiff_t step) {
					i -= step;
					return *this;
				}

				size_t operator- (reverse_iterator i_) {
					size_t diff = i_ - i;
					return diff;
				}

				T* operator-> () const{
					return &(i->second);
				}

				T operator[](int index){
					i += index;
					return i->second;
				}

				// ---
				// comparators
				// ---
				bool operator!= (reverse_iterator i_){
					return pC != i_.pC;
				}

				bool operator== (reverse_iterator i_){
					return pC == i_.pC;
				}

				bool operator> (reverse_iterator i_){
					return pC > i_.pC;
				}

				bool operator< (reverse_iterator i_){
					return pC < i_.pC;
				}

				bool operator<= (reverse_iterator i_){
					return pC <= i_.pC;
				}

				bool operator>= (reverse_iterator i_){
					return pC >= i_.pC;
				}

				// ---
				// other methods
				// ---
				size_t getAge(){return i->first;};
		};




		// ------------------
		// constructor
		// ------------------
		Ager(size_t maxage){
			_maxage = maxage;
		}

		// ------------------
		// iterator methods
		// ------------------
		iterator begin() {return iterator(this,container.begin()); }
    	iterator end() { return iterator(); }
    	reverse_iterator rbegin() { return reverse_iterator(this,container.rbegin()); }
    	reverse_iterator rend() { return reverse_iterator(); }


		// -------------------------------
		// container "wrap-in methods"
		// -------------------------------
		void push_back(const T& x){
			pair<size_t,T> element(0,x);
			container.push_back(element);
		}
	
		void pop_back(){
			container.pop_back();
		}
		
		void push_front(const T& x){
			pair<size_t,T> element(0,x);
			container.push_front(element);
		}
		
		void pop_front(){
			container.pop_front();
		}

		size_t size(){
			return container.size();
		}

		void erase(const iterator& i){
			i.erase();
		}

		void erase(const reverse_iterator& ri){
			ri.erase();
		}


		// -----------------------
		// operator overloading
		// -----------------------
        T operator[](int index)
        {        	
			typename CON::iterator i = container.begin();
			for (int counter=0;counter < index ;counter++ )
			{
				i++;
			}
			return i->second;
        }


		// -----------------
		// other methods
		// -----------------
		void tick(){
			CON container_tmp;
			
			while(!container.empty()){
				size_t age;
				pair<size_t,T> element;

				element = container.front();
				
				age = element.first;
				age++;
				element.first = age;
				
				container.pop_front();
				
				if (age<_maxage)
					container_tmp.push_back(element);
			
			}
			container = container_tmp;
		}

		friend class iterator;
		friend class reverse_iterator;

	private:
		size_t _maxage;
		CON container;
		Ager<T,CON> *pC;

};



#endif

