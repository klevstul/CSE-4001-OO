#ifndef PRISONCELL_PROTECTOR
#define PRISONCELL_PROTECTOR


class PrisonCell{

	public:
		PrisonCell() : door(0) {}
		
		void toggle(){
			if (door==0)
				door = 1;
			else
				door = 0;
		}

		bool isDoorOpen(){ return door == 1; }

	private:
		int door; // 0: close, 1: open
};

#endif
