#include "ager.h"

int main()
{
	Ager<int> ager(3);

	ager.push_back(10);
	ager.tick();
	ager.push_back(11);
	ager.tick();
	ager.push_back(12);
	ager.tick();
	ager.push_back(13);
	ager.push_back(14);
	ager.push_back(15);
	ager.tick();



	cout << "ager contains " << ager.size() << " elements:" << endl;

//Ager<int>::iterator i = ager.begin();
//cout << i.getAge() << endl;

	for (Ager<int>::iterator i = ager.begin() ; i != ager.end() ; ++i)
		cout << i.getAge() << ":" << *i << endl;
	cout << endl;


	for (Ager<int>::reverse_iterator r = ager.rbegin(); r != ager.rend() ; ++r)
		cout << r.getAge() <<":"<< *r << endl;

	return EXIT_SUCCESS;
}

/** SHOULD PRINT: 

ager contains 4 elements:
2:12
1:13
1:14
1:15

1:15
1:14
1:13
2:12

*/
